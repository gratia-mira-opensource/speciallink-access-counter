<?php
/**
 * @version		1.0.0
 * @package		Joomla
 * @subpackage	Content
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 * https://docs.joomla.org/J3.x:Creating_a_content_plugin
 */
 
// Kein direkter Zugriff erlauben
defined( '_JEXEC' ) or die( 'Restricted access' );


class plgSystemSpeciallinkAccessCounter extends JPlugin {

    public function __construct(&$subject, $config) {
		// Calling the parent Constructor
		parent::__construct($subject, $config);
	}
	
	public static function onAfterDispatch() {

        $plugin = JPluginHelper::getPlugin('system', 'speciallinkaccesscounter');
        $params = new JRegistry($plugin->params);
        $url = $params->get('url','string');
        $kampagnenname = $params->get('kampagnenname','string');

        $db = JFactory::getDbo();

        // Prüfen ob Link in der Datenbank ist
        if (!isset($_SESSION['SpeciallinkAccessCounter'])) {
            $query = $db->getQuery(true);
        
            // Abfrage erzeugen, um zu prüfen, ob der Eintrag noch gemacht werden muss
            $query->select(array('url'));
            $query->from('#__speciallinkaccesscounter');
            $query->where('url = ' . $db->quote($url));
            
            // Das Query erzeugen
            $db->setQuery($query);
            
            // Ist ein Datensatz vorhanden?
            $Eintrag = $db->loadResult();

            // Sessionswert setzen
            $_SESSION['SpeciallinkAccessCounter'] = true;
        
            // Eintrag wenn nötig neu machen
            if (empty($Eintrag)) {
                // Neues Query-Objekt
                $query = $db->getQuery(true);
                
                $Spalten = array('url','alias', 'direkt', 'total');

                // Werte einfügen
                $Werte = array($db->quote($url),$db->quote($kampagnenname),$db->quote(0),$db->quote(0));

                // Die Abfrage formulieren
                $query->insert($db->quoteName('#__speciallinkaccesscounter'));
                $query->columns($db->quoteName($Spalten));
                $query->values(implode(',', $Werte));
                
                // Query erzeugen und in DB abspeichern
                $db->setQuery($query);
                $db->execute();
            }
            
            // Alte Einträge löschen
            $query = $db->getQuery(true);

            $query->delete($db->quoteName('#__speciallinkaccesscounter'));
            $query->where('url != ' . $db->quote($url));

            $db->setQuery($query);
            $db->execute();

        }

		$requesturl = $_SERVER['REQUEST_URI'];

        // Zähler erhöhen
        if ($url ==  $requesturl) {
        
            // Neues Query-Objekt
            $query = $db->getQuery(true);

			// Was soll geändert werden
			// Nur wenn es ein Direktzugriff ist.
            if (empty($_SERVER['HTTP_REFERER'])) {
                $qFelder = array($db->quoteName('direkt') . ' = direkt + 1',
                            $db->quoteName('total') . ' =  total + 1');
            } else {
				$qFelder = array($db->quoteName('total') . ' =  total + 1');
			}

            // Wo soll es geändert werden
            $qBedingungen = array($db->quoteName('url') . ' = ' . $db->quote($url));

            $query->update($db->quoteName('#__speciallinkaccesscounter'))->set($qFelder)->where($qBedingungen);

            // Query erzeugen und in DB abspeichern
            $db->setQuery($query);
            $db->execute();
        }

        // Zähler anzeigen
        if (isset($_GET['speciallink'])) {
            $query = $db->getQuery(true);
        
            // Abfrage erzeugen, um zu prüfen, ob der Eintrag noch gemacht werden muss
            $query->select(array('alias','url','direkt','total','datum_start'));
            $query->from('#__speciallinkaccesscounter');
            $query->where('url = ' . $db->quote($url));
            
            // Das Query erzeugen
            $db->setQuery($query);
            
            // Ist ein Datensatz vorhanden?
            $Info = $db->loadObject();
            
            echo '<div style="background-color: orange; color: white; text-align: center; font-family: sans-serif; padding: 5px;">';
            if ($Info) {
                echo    '<span title="'. $Info->url . '"><b>' . $Info->alias . '</b></span><br><br>' .
                        'Direkte Zugriffe: ' . $Info->direkt . '<br>' .
                        'Alle Zugriffe: ' . $Info->total . '<br><br>' .
                        '(seit dem ' . date('d.m.Y', strtotime($Info->datum_start)) . ')<br>' .
                        'Aktuelle Seite: ' . str_replace(array('?speciallink=anzeigen','&speciallink=anzeigen'), '', $requesturl);
            } else {
                echo    'Bitte Plugin "Speciallink Access Counter" einstellen und anschließend Browserfenster neu öffen. <br> Aktuelle Seite: ' . str_replace(array('?speciallink=anzeigen','&speciallink=anzeigen'), '', $requesturl);
            }
            echo '</div>';
        }
	}
}
