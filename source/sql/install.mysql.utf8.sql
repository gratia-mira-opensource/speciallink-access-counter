--
-- install
--

CREATE TABLE IF NOT EXISTS `#__speciallinkaccesscounter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `direkt` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `datum_start` date NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;