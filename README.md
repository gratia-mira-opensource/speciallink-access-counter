# Anleitung
Plugin installieren und Einstellungen eintragen.   
Mit den Parametern "?speciallink=anzeigen" kann man den exakten Pfad anzeigen lassen, wie er auch funktioniert. Da es ein Plugin ist, funktioniert es auf allen Seiten.
Später kann man sich mit denselben Parametern die Zahl der Aufrufe anezeigen lassen.
# Abeitsweise
Das Plugin vergleicht den URI-Request mit dem vorgegebenen Link. Ist er identisch, wird der Zähler erhöht. Es wird zwischen direkt aufgerufenen Seiten und und allen Aufrufen (auch über die Normale Navigation oder Google) unterschieden.   
Werden die Einstellungen geändert, werden auch die Klickzahlen der vorherigen Einstellungen gelöscht.   
   
Vergliechen wird der gesamte Link (inkl. Parameter).
# Beobachtungen
Gewisse QR Codeleser, rufen schon vor dem eigentlichen die Seite auf. Es kann also sein, dass pro Aufruf zwei Punkte vergeben werden.
